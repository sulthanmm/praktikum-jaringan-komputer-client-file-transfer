﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
namespace Client_Socket
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IPAddress[] ipAddress = Dns.GetHostAddresses("localhost");
                IPEndPoint ipEnd = new IPEndPoint(ipAddress[1], 5656);
                Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                clientSocket.Connect(ipEnd);

				string fileName = "test.exe";// File Name to be send
                string filePath = @"C:\"; // File path
                
                byte[] fileNameByte = Encoding.ASCII.GetBytes(fileName);
                byte[] fileData = File.ReadAllBytes(filePath + fileName);
                byte[] clientData = new byte[4 + fileNameByte.Length + fileData.Length];
				
                fileNameByte.CopyTo(clientData, 0);
                fileData.CopyTo(clientData, 4 + fileNameByte.Length);

                clientSocket.Send(clientData);
                Console.WriteLine("{0} has been sent.", fileName);
                
                clientSocket.Close();
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }
    }
}